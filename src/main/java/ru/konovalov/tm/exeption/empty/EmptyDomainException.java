package ru.konovalov.tm.exeption.empty;

import org.jetbrains.annotations.NotNull;
import ru.konovalov.tm.exeption.AbstractException;

public class EmptyDomainException extends AbstractException {

    @NotNull
    public EmptyDomainException() {
        super("Error. Domain is empty");
    }

}