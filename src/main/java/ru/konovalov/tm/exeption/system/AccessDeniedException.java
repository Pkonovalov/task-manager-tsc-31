package ru.konovalov.tm.exeption.system;

import ru.konovalov.tm.exeption.AbstractException;

public class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error. Access denied.");
    }

}
